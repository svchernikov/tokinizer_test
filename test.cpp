/*
 * test.cpp
 *
 *  Created on: Feb 9, 2018
 *      Author: sergey
 */



#include "ttokinizer.h"
#include <iostream>
#include <string>

template<typename T>
using array3 = std::array<int, 3>;

int main() {


	auto t = ttokenizer::tokenizer<'\t'>(std::string("1.33\t\t\t\t-2\tstring\t100"),
										 ttokenizer::typeList2type<float, int, std::string, unsigned>(),
										 ttokenizer::idxList2type<1,2,3,4>());

	std::cout << "Tokens read: " << t.n << "; Tokens: " <<
			std::get<0>(t.tokens) << " " <<
			std::get<1>(t.tokens) << " " <<
			std::get<2>(t.tokens) << " " <<
			std::get<3>(t.tokens) << std::endl;
	return 1;
}
