/*
 * ttokinizer.h
 *
 *  Created on: Feb 9, 2018
 *      Author: sergey
 */

#pragma once

#include <tuple>
#include <cstdlib>

namespace ttokenizer {

//structure that describes read tokens
template <typename ...Ts>
struct Tokens {
	using Ttuple = std::tuple<Ts...>;

	unsigned n; 		  //number of read tokens
	std::tuple<Ts...> tokens; //tokens
};

template<unsigned...> struct idxList2type{};
template<typename...> struct typeList2type{};

namespace impl {

template<typename Tret>
Tret stox(std::string &&str);

template<>
std::string stox<std::string>(std::string &&str) {
	return std::move(str);
}

template<>
int stox<int>(std::string&& str) {
	return std::atoi(str.c_str());
}

template<>
unsigned stox<unsigned>(std::string&& str) {
	return static_cast<unsigned>(std::atoll(str.c_str()));
}

template<>
double stox<double>(std::string&& str) {
	return std::atof(str.c_str());
}

template<>
float stox<float>(std::string&& str) {
	return static_cast<float>(std::atof(str.c_str()));
}


struct null_type {};
template<unsigned Idx, unsigned ...Idxs> struct cutIdxList2type{};
template<typename T, typename ...Ts> struct cutTypeList2type{};


//common template to add token to tuple
template <
		char Delim,
		unsigned Tok_num,
		unsigned I,
		unsigned Prev_idx,
		typename Ttokens,
		typename Tstring,
		typename T, typename ...Ts, template <typename, typename...> class CTs,
		unsigned Idx, unsigned ...Idxs, template <unsigned, unsigned...> class Cidx
		>
void tokenizer_impl(const Tstring &str, size_t start_pos, Ttokens &_t, const CTs<T, Ts...>&, const Cidx<Idx, Idxs...>&) {
	size_t end_pos = str.find_first_of(Delim, start_pos);
	//skip tokens
	for(unsigned i = 1; i < Idx - Prev_idx; ++i) {
		start_pos = str.find_first_not_of(Delim, end_pos);
		end_pos = str.find_first_of(Delim, start_pos);
	}

	//if it is end of the string
	if(start_pos == Tstring::npos)
		return;

	//add tuple element type
	using elem_type = typename std::tuple_element<Tok_num-I, typename Ttokens::Ttuple>::type;
	std::get<Tok_num-I>(_t.tokens) = stox<elem_type>(
			(start_pos != std::string::npos)?
			str.substr(start_pos, end_pos - start_pos):
			std::string(""));

	//increment number of read tokens
	++_t.n;

	start_pos = str.find_first_not_of(Delim, end_pos);

	tokenizer_impl<Delim, Tok_num, I-1, Idx>(str,
											 start_pos, _t,
											 cutTypeList2type<Ts...>(),
											 cutIdxList2type<Idxs...>()
											 );
}

//termination specification (by both type and index)
template <
		char Delim,
		unsigned Tok_num,
		unsigned I,
		unsigned Prev_idx,
		typename Ttokens,
		typename Tstring
		>
void tokenizer_impl(const Tstring &, size_t, Ttokens &, const cutTypeList2type<null_type>&, const cutIdxList2type<0>&) {}

} //end impl


template<
		char Delim = '\t',
		typename Tstring,
		typename ...Ts, template <typename...> class CTs,
		unsigned ...Idxs, template <unsigned...> class CIdxs
		>
Tokens<Ts...> tokenizer(const Tstring &str, const CTs<Ts...>&, const CIdxs<Idxs...>&) {
	Tokens<Ts...> _t; //output tokens struct
	_t.n = 0; //init number of read tokens
	static unsigned const Tok_num = sizeof...(Ts);
	impl::tokenizer_impl<Delim, Tok_num, Tok_num, 0>(str, 0, _t,
													 impl::cutTypeList2type<Ts..., impl::null_type>(),
													 impl::cutIdxList2type<Idxs..., 0>());
	return _t;
}

} //end ttokenizer
